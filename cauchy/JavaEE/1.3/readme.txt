http://java.sun.com/dtd/

1. J2EE DTDs
September 16, 2001  connector_1_0.dtd   XML DTD for a Connector 1.0 resource adapter (raa) module
September 16, 2001  jspxml.xsd          XML Schema for a JSP Page
September 16, 2001  jspxml.dtd          XML DTD for a JSP Page
September 16, 2001  web-jsptaglibrary_1_2.dtd   DTD defining the JavaServer Pages 1.2 Tag Library descriptor (.tld) (XML) file format/syntax
September 16, 2001  web-app_2_3.dtd     XML DTD for a Servlet 2.3 web-app (war) module
September 16, 2001  application-client_1_3.dtd  XML DTD for J2EE 1.3 app-client (app-client.jar) module
September 16, 2001  application_1_3.dtd XML DTD for a J2EE 1.3 application (ear) file
September 16, 2001  ejb-jar_2_0.dtd     XML DTD for an EJB 2.0 (ejb.jar) module

2. J2SE DTDs
April 26, 2006      JNLP-1.5.dtd    XML DTD for JNLP 1.5
April 26, 2006      JNLP-6.0.dtd    XML DTD for JNLP 6.0
January 19, 2005    properties.dtd  XML DTD for java.util.Properties
September 3, 2001   logger.dtd      XML DTD for java.util.logging log records
April 7, 2002       preferences.dtd XML DTD for java.util.preferences preferences data

3. Java Data Objects DTDs
May 5, 2002         jdo_1_0.dtd     XML DTD for the JDO 1.0 Metadata

4. JavaServer Faces DTDs
May 28, 2004    web-facesconfig_1_1.dtd     XML DTD for the JavaServer Faces 1.1 Application Configuration File
March 11, 2004  web-facesconfig_1_0.dtd     XML DTD for the JavaServer Faces 1.0 Application Configuration File

5. JSLEE DTDs
March 5, 2004   slee-deployable-unit_1_0.dtd    XML DTD for the JSLEE 1.0 deployable unit jar file deployment descriptor
March 5, 2004   slee-event-jar_1_0.dtd          XML DTD for the JSLEE 1.0 event component jar file deployment descriptor
March 5, 2004   slee-profile-spec-jar_1_0.dtd   XML DTD for the JSLEE 1.0 profile specification component jar file deployment descriptor
March 5, 2004   slee-resource-adaptor-jar_1_0.dtd       XML DTD for the JSLEE 1.0 resource adaptor component jar file deployment descriptor
March 5, 2004   slee-resource-adaptor-type-jar_1_0.dtd  XML DTD for the JSLEE 1.0 resource adaptor type component jar file deployment descriptor
March 5, 2004   slee-sbb-jar_1_0.dtd    XML DTD for the JSLEE 1.0 SBB component jar file deployment descriptor
March 5, 2004   slee-service_1_0.dtd    XML DTD for the JSLEE 1.0 service component deployment descriptor
