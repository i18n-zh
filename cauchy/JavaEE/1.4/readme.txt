http://java.sun.com/xml/ns/j2ee/

1. J2EE 1.4 Schema Resources
November 24, 2003   web-jsptaglibrary_2_0.xsd   JSP Taglibrary Deployment Descriptor Schema     Final Release
November 24, 2003   ejb-jar_2_1.xsd     Enterprise JavaBeans Deployment Descriptor Schema       Final Release
November 24, 2003   connector_1_5.xsd   Connector Deployment Descriptor Schema                  Final Release
February, 2004      web-app_2_4.xsd     Servlet Deployment Descriptor Schema                    Maintenance Release
November 24, 2003   jsp_2_0.xsd         JavaServer Pages Deployment Descriptor Schema           Final Release
November 24, 2003   j2ee_1_4.xsd        J2EE 1.4 definitions file that contains common schema components    Final Release
November 24, 2003   application-client_1_4.xsd  Application Client schema   Final Release
November 24, 2003   application_1_4.xsd         Application schema  Final Release
November 24, 2003   http://www.ibm.com/webservices/xsd/j2ee_web_services_1_1.xsd           Web services schema          Final Release
November 24, 2003   http://www.ibm.com/webservices/xsd/j2ee_web_services_client_1_1.xsd    Web services client schema   Final Release
November 24, 2003   http://www.ibm.com/webservices/xsd/j2ee_jaxrpc_mapping_1_1.xsd         JAX-RPC mapping schema       Final Release
-                   http://www.w3.org/2001/xml.xsd      The J2EE schemas use some common definitions provided and published by W3C  -
