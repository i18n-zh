#include <arpa/inet.h>
#include <grp.h>
#include <pwd.h>
#include <signal.h>
#include <syslog.h>
#include <unistd.h>

#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>

/*
 * gcc -O2 -Wall -o /usr/sbin/authgw authgw.c
 *
 * groupadd -g 72 authgw
 * %authgw ALL=(root)NOPASSWD: /usr/sbin/authgwctl
 *
 * echo authgw:fenghuisi@192.168.2.52 | awk -F ":|@" '{print $3}'
 *
 * env[1]=<LOGNAME=test_user_01>
 * env[6]=<SSH_CLIENT=192.168.2.52 1316 22>
 */

#define die(msg) { perror(msg); syslog(LOG_ERR, msg); exit(EXIT_FAILURE); }

const char *path_authgw_conf =  "/etc/authgw/authgw.conf";
const char *path_authgw_rules = "/etc/authgw/authgw.rules";
const char *path_user_dir =     "/etc/authgw/users";
const char *path_ban_dir =      "/etc/authgw/banned";
const char *path_pidfile =      "/var/authgw";
const char *path_authgw_shell = "/usr/sbin/authgw";
const char *path_authgw_ctl =   "/usr/sbin/authgwctl";
const char *path_sudo =         "/usr/bin/sudo";

FILE    *pidfp = NULL;
int     pidfd = -1;
char    luser[LOGIN_NAME_MAX];  /* 256, username */
char    ipsrc[HOST_NAME_MAX];     /* 64, ip as a string */
char    pidfile[PATH_MAX + HOST_NAME_MAX];    /* 4096+64, we save pid in this file. */

volatile sig_atomic_t want_death;
static void need_death(int signo);
static void  do_death(int);

void setproctitle_init(int argc, char *argv[], char *envp[]);
int setproctitle(const char *fmt, ...);

int main(int argc, char *argv[], char *env[])
{
    int lockcnt = 0, n;
    char *cp, cmd_buffer[256];
    struct in6_addr ina;
    struct passwd *pw;
    uid_t uid;

    openlog("authgw", LOG_CONS | LOG_PID, LOG_USER);

    if ((cp = getenv("SSH_TTY")) == NULL) {
        syslog(LOG_ERR, "non-interactive session connection for authpf");
        exit(1);
    }

    if ((cp = getenv("SSH_CLIENT")) == NULL) {
        syslog(LOG_ERR, "cannot determine connection source");
        exit(1);
    }

    if (strlen(cp) >= sizeof(ipsrc)) {
        syslog(LOG_ERR, "SSH_CLIENT variable too long");
        exit(1);
    }

    strncpy(ipsrc, cp, sizeof(ipsrc));
    ipsrc[sizeof(ipsrc) - 1] = '\0';
    cp = strchr(ipsrc, ' ');
    if (!cp) {
        syslog(LOG_ERR, "corrupt SSH_CLIENT variable %s", ipsrc);
        exit(1);
    }
    *cp = '\0';
    if (inet_pton(AF_INET, ipsrc, &ina) != 1 &&
        inet_pton(AF_INET6, ipsrc, &ina) != 1) {
        syslog(LOG_ERR,
            "cannot determine IP from SSH_CLIENT %s", ipsrc);
        exit(1);
    }

    uid = getuid();
    pw = getpwuid(uid);
    if (pw == NULL) {
        syslog(LOG_ERR, "cannot find user for uid %u", uid);
        goto die;
    }

/*
    if (strcmp(pw->pw_shell, path_authgw_shell)) {
        syslog(LOG_ERR, "wrong shell for user %s, uid %u",
            pw->pw_name, pw->pw_uid);
        goto die;
    }
*/

    if (strlen(pw->pw_name) >= sizeof(luser)) {
        syslog(LOG_ERR, "username too long: %s", pw->pw_name);
        exit(1);
    }

    strncpy(luser, pw->pw_name, sizeof(luser));
    luser[sizeof(luser) - 1] = '\0';

    /* Make our entry in /var/authpf as ipaddr or username */
    n = snprintf(pidfile, sizeof(pidfile), "%s/%s", path_pidfile, ipsrc);
    if (n < 0 || n >= sizeof(pidfile)) {
        syslog(LOG_ERR, "path to pidfile too long");
        goto die;
    }

    signal(SIGTERM, need_death);
    signal(SIGINT, need_death);
    signal(SIGALRM, need_death);
    signal(SIGPIPE, need_death);
    signal(SIGHUP, need_death);
    signal(SIGQUIT, need_death);
    signal(SIGTSTP, need_death);

    do {
        int save_errno, otherpid = -1;
        char otherluser[LOGIN_NAME_MAX];

        if ((pidfd = open(pidfile, O_RDWR|O_CREAT, 0644)) == -1 ||
            (pidfp = fdopen(pidfd, "r+")) == NULL) {
            if (pidfd != -1)
                close(pidfd);
            syslog(LOG_ERR, "cannot open or create %s: %s", pidfile,
                strerror(errno));
            goto die;
        }

        if (flock(fileno(pidfp), LOCK_EX|LOCK_NB) == 0)
            break;
        save_errno = errno;

        /* Mark our pid, and username to our file. */

        rewind(pidfp);
        /* 31 == MAXLOGNAME - 1 */
        if (fscanf(pidfp, "%d\n%31s\n", &otherpid, otherluser) != 2)
            otherpid = -1;
        syslog(LOG_INFO, "tried to lock %s, in use by pid %d: %s",
            pidfile, otherpid, strerror(save_errno));

        if (otherpid > 0) {
            syslog(LOG_INFO,
                "killing prior auth (pid %d) of %s by user %s",
                otherpid, ipsrc, otherluser);
            if (kill((pid_t) otherpid, SIGTERM) == -1) {
                syslog(LOG_INFO,
                    "could not kill process %d: (%m)",
                    otherpid);
            }
        }

        /*
         * We try to kill the previous process and acquire the lock
         * for 10 seconds, trying once a second. if we can't after
         * 10 attempts we log an error and give up.
         */
        if (want_death || ++lockcnt > 10) {
            if (!want_death)
                syslog(LOG_ERR, "cannot kill previous authpf (pid %d)",
                    otherpid);
            fclose(pidfp);
            pidfp = NULL;
            pidfd = -1;
            goto dogdeath;
        }
        sleep(1);

        /* re-open, and try again. The previous authpf process
         * we killed above should unlink the file and release
         * it's lock, giving us a chance to get it now
         */
        fclose(pidfp);
        pidfp = NULL;
        pidfd = -1;
    } while (1);

    /* whack the group list
    gid = getegid();
    if (setgroups(1, &gid) == -1) {
        syslog(LOG_INFO, "setgroups: %s", strerror(errno));
        do_death(0);
    } */

    /* revoke privs
    uid = getuid();
    if (setresuid(uid, uid, uid) == -1) {
        syslog(LOG_INFO, "setresuid: %s", strerror(errno));
        do_death(0);
    } */

    /* We appear to be making headway, so actually mark our pid */
    rewind(pidfp);
    fprintf(pidfp, "%ld\n%s\n", (long)getpid(), luser);
    fflush(pidfp);
    (void) ftruncate(fileno(pidfp), ftello(pidfp));

    /* snprintf(cmd_buffer, sizeof(cmd_buffer), "%s %s -A %s -u %s", path_sudo, path_authgw_ctl, ipsrc, luser); */
    snprintf(cmd_buffer, sizeof(cmd_buffer), "%s %s -A %s", path_sudo, path_authgw_ctl, ipsrc);
    if(0 != system(cmd_buffer)) {
        syslog(LOG_ERR, "exec %s fail", cmd_buffer);
        printf("exec %s fail", cmd_buffer);
        sleep(60);
        do_death(0);
    }

    while (1) {
        printf("\nHello %s, you are authenticated from host \"%s\".\n", luser, ipsrc);
        syslog(LOG_NOTICE, "Hello %s, you are authenticated from host \"%s\".\n", luser, ipsrc);

        setproctitle("%s@%s", luser, ipsrc);
        while (1) {
            sleep(1);
            if (want_death)
                do_death(1);
        }
    }

    /* SHOULD NOT REACHED */
dogdeath:
    printf("\r\n\r\nSorry, this service is currently unavailable due to ");
    printf("technical difficulties\r\n\r\n");
    printf("\r\nYour authentication process (user %s, pid %ld) was unable to run\n",
        luser, (long)getpid());
    sleep(180); /* them lusers read reaaaaal slow */

die:
    do_death(0);

    return 0;
}


/* signal handler that makes us go away properly */
static void need_death(int signo)
{
    want_death = 1;
}


/* function that removes our stuff when we go away. */
static void do_death(int active)
{
    int ret = 0;

    if (active) {
        char cmd_buffer[256];

        /* snprintf(cmd_buffer, sizeof(cmd_buffer), "%s %s -D %s -u %s", path_sudo, path_authgw_ctl, ipsrc, luser); */
        snprintf(cmd_buffer, sizeof(cmd_buffer), "%s %s -D %s", path_sudo, path_authgw_ctl, ipsrc);
        if(0 != system(cmd_buffer)) {
            syslog(LOG_ERR, "exec %s fail", cmd_buffer);
            printf("exec %s fail", cmd_buffer);
            sleep(60);
        }
    }

    if (pidfile[0] && pidfd != -1)
        if (unlink(pidfile) == -1)
            syslog(LOG_ERR, "cannot unlink %s (%m)", pidfile);
    exit(ret);
}

static char *title_buffer = 0;
static size_t title_buffer_size = 0;

void setproctitle_init(int argc, char *argv[], char *envp[])
{
    if (!title_buffer || !title_buffer_size) {
        int i;
        char *begin_of_buffer = 0, *end_of_buffer = 0;

        for (i = 0; i < argc; ++i) {
            if (!begin_of_buffer)
                begin_of_buffer = argv[i];
            if (!end_of_buffer || end_of_buffer + 1 == argv[i])
                end_of_buffer = argv[i] + strlen(argv[i]);
        }

        for (i = 0; envp[i]; ++i) {
            if (!begin_of_buffer)
                begin_of_buffer = envp[i];
            if (!end_of_buffer || end_of_buffer + 1 == envp[i])
                end_of_buffer = envp[i] + strlen(envp[i]);
        }

        title_buffer = begin_of_buffer;
        title_buffer_size = end_of_buffer - begin_of_buffer;

        printf("title_buffer_size: %d\n", (int)title_buffer_size);
    }
}

int setproctitle(const char *fmt, ...)
{
    if (title_buffer_size && fmt) {
        va_list ap;
        ssize_t written;

        va_start(ap, fmt);
        written = vsnprintf(title_buffer, title_buffer_size, fmt, ap);
        va_end(ap);

        if (written < 0)
            return -1;

        if(title_buffer_size > written)
            memset(title_buffer + written, '\0', title_buffer_size - written);
        title_buffer[title_buffer_size -1] = '\0';
    }

    return 0;
}
