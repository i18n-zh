#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#
# ps -ax | grep authgw |  awk -F '@' '{print $2}' | awk '{print $1}' | sort -u -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4
#
# cp authgwctl.py /usr/sbin/authgwctl
#
# /usr/sbin/authgwctl -A ip
# /usr/sbin/authgwctl -D ip
# /usr/sbin/authgwctl -F
#

import os, sys, getopt, socket, syslog
from subprocess import Popen, PIPE

path_authgw_conf =      "/etc/authgw/authgw.conf"
path_authgw_rules =     "/etc/authgw/authgw.rules"
path_user_dir =         "/etc/authgw/users"
path_ban_dir =          "/etc/authgw/banned"
path_pidfile =          "/var/authgw"
path_authgw_shell =     "/usr/sbin/authgw"
path_authgw_ctl =       "/usr/sbin/authgwctl"
path_cat =              '/bin/cat'
path_iptables =         '/sbin/iptables'
path_iptables_restore = '/sbin/iptables-restore'

def usage():
    print >> sys.stderr, "Usage:  %s [OPTIONS] [IP]" % (sys.argv[0])
    print >> sys.stderr, """
OPTIONS may be some of:
    -A, --append ip     Append one ip to the end of the forward chain.
    -D, --delete ip     Delete one ip from the forward chain.
    -F, --flush         Flush the rules.
    -H, --help          Output this message
"""
    sys.exit(1)

def check_ip(ip):
    err = ""
    try:
        socket.inet_pton(socket.AF_INET, ip)
        return
    except socket.error, err:
        pass

    try:
        socket.inet_pton(socket.AF_INET6, ip)
        return
    except socket.error, err:
        print "Illegal IP address string: " + ip

    usage()

def user_gw_op(op, ip):
    check_ip(ip)

    f = open(path_authgw_rules, 'r')
    for line in f:
        line = line.strip()
        if line[:3] == '-t ':
            cmd = path_iptables + ' ' + (line % (op, ip))
            syslog.syslog(syslog.LOG_ERR, "User cmd: " + cmd);

            p = Popen(cmd, shell=True, stderr=PIPE)
            (pid, rc) = os.waitpid(p.pid, 0)
            if rc != 0:
                syslog.syslog(syslog.LOG_ERR, "Popen fail cmd: " + cmd);
                for err in p.stderr:
                    syslog.syslog(syslog.LOG_ERR, "Popen fail msg: " + err);
                sys.exit(1)
    f.close();

    if op == '-A':
        syslog.syslog(syslog.LOG_NOTICE, "Append " + ip + " to iptables");
    else:
        syslog.syslog(syslog.LOG_NOTICE, "Remove " + ip + " from iptables");

def flush_rules():
    cmd = path_cat + ' ' + path_authgw_conf + " | " + path_iptables_restore
    syslog.syslog(syslog.LOG_ERR, "Flush cmd: " + cmd);

    p = Popen(cmd, shell=True, stderr=PIPE)
    (pid, rc) = os.waitpid(p.pid, 0)
    if rc != 0:
        syslog.syslog(syslog.LOG_ERR, "Flush fail cmd: " + cmd);
        for err in p.stderr:
            syslog.syslog(syslog.LOG_ERR, "Flush fail msg: " + err);

    p1 = Popen(["ps", "-ax"], stdout=PIPE)
    p2 = Popen(["grep", "authgw"], stdin=p1.stdout, stdout=PIPE)
    p3 = Popen(["awk", "-F", "@", "{print $2}"], stdin=p2.stdout, stdout=PIPE)
    p4 = Popen(["awk", "{print $1}"], stdin=p3.stdout, stdout=PIPE)
    p5 = Popen(["sort", "-u", "-n", "-t", ".", "-k", "1,1", "-k", "2,2", "-k", "3,3", "-k", "4,4"], stdin=p4.stdout, stdout=PIPE)
    output = p5.stdout
    filter_auth=""

    for ip in output:
        if(ip[0] >= '1' and ip[0] <= '9'):
            ip = ip.strip()
            f = open(path_authgw_rules, 'r')
            for line in f:
                line = line.strip()
                if line[:3] == '-t ':
                    cmd = path_iptables + ' ' + (line % (op, ip))
                    syslog.syslog(syslog.LOG_ERR, "User cmd: " + cmd);

                    p = Popen(cmd, shell=True, stderr=PIPE)
                    (pid, rc) = os.waitpid(p.pid, 0)
                    if rc != 0:
                        syslog.syslog(syslog.LOG_ERR, "Popen fail cmd: " + cmd);
                        for err in p.stderr:
                            syslog.syslog(syslog.LOG_ERR, "Popen fail msg: " + err);
            f.close();

    syslog.syslog(syslog.LOG_ERR, 'Flush rules success !');

def main(argv):
    if len(sys.argv) < 2: usage()

    syslog.openlog('authgwctl', syslog.LOG_CONS | syslog.LOG_PID, syslog.LOG_USER);

    args = sys.argv[1:]
    try:
        opts, args = getopt.getopt(args, 'A:D:F', ['append=','delete=', 'flush'])
        for opt, arg in opts:
            if opt in ('-A', '--append'):
                user_gw_op('-A', arg)
                sys.exit(0)
            elif opt in ('-D', '--delete'):
                user_gw_op('-D', arg)
                sys.exit(0)
            elif opt in ('-F', '--flush'):
                flush_rules()
                sys.exit(0)
    except getopt.GetoptError, err:
        print str(err)

    usage()

if __name__ == '__main__':
    main(sys.argv)
