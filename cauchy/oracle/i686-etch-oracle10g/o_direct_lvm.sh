#
# Use block device for Oracle files, Oracle 10g Release 2 in Linux 2.6
# automatically opens device using O_DIRECT.
#
# fs.aio-max-nr = 65536
# fs.aio-nr = 15360
#
# For Oracle 10g Release 2 in Linux 2.6 it is not recommended to use raw
# devices but to use block devices instead. Raw I/O is still available in
# Linux 2.6 but it is now a deprecated interface. It has been replaced by
# the O_DIRECT flag which can be used for opening block devices to bypass
# the OS cache. Unfortunately, Oracle Clusterware R2 OUI has not been updated
# and still requires raw devices or a Cluster File System.
#
# udev, device mapper(supports logical volume management)
# Ownership and permissions are set and persisted automatically
# udev - /dev/ and hotplug management daemon
# libdevmapper-dev - The Linux Kernel Device Mapper header files
#
# Boot options: elevator=deadline
#

chmod 0660          /dev/mapper/vg00-10g_*
chown oracle:dba    /dev/mapper/vg00-10g_*

ln -sf /dev/mapper/vg00-10g_spfile      /dev/raw/10g_spfile     # 8
ln -sf /dev/mapper/vg00-10g_ctrl_1      /dev/raw/10g_ctrl_1     # 128
ln -sf /dev/mapper/vg00-10g_ctrl_2      /dev/raw/10g_ctrl_2     # 128
ln -sf /dev/mapper/vg00-10g_redo1_1     /dev/raw/10g_redo1_1    # 128
ln -sf /dev/mapper/vg00-10g_redo1_2     /dev/raw/10g_redo1_2    # 128
ln -sf /dev/mapper/vg00-10g_undo_01     /dev/raw/10g_undo_01    # 256
ln -sf /dev/mapper/vg00-10g_system      /dev/raw/10g_system     # 384
ln -sf /dev/mapper/vg00-10g_tmp         /dev/raw/10g_tmp        # 64
ln -sf /dev/mapper/vg00-10g_users       /dev/raw/10g_users      # 64
ln -sf /dev/mapper/vg00-10g_sysaux      /dev/raw/10g_sysaux     # 128
