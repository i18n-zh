modprobe raw
mkdir /dev/raw
mknod /dev/raw/raw01 c 162 1
raw /dev/raw/raw01 /dev/mapper/vg00-db_01

# chown oracle:dba /dev/raw/rawn
# chmod 660 /dev/raw/rawn

1. software
# apt-get install gcc make binutils lesstif2 rpm
# mkdir /var/lib/rpm && rpm --initdb

**************** libstdc++-libc6.1-1.so.2 ****************
# dpkg -i libstdc++2.10_2.95.2-14_i386.deb
/usr/lib# ln -sf libstdc++-3-libc6.1-2-2.10.0.so libstdc++-libc6.1-1.so.2

**************** libwait ****************
p3006854_9204_LINUX.zip
gcc -shared -fpic -O2 -o libcwait.so libcwait.c
export LD_PRELOAD=/path/to/libcwait.so

**************** jre ****************
export LD_ASSUME_KERNEL=2.4.21

ln -s /usr/bin/awk /bin/awk
ln -s /usr/bin/rpm /bin/rpm
ln -s /usr/bin/basename /bin/basename # Suggested by Giuseppe Sacco
ln -s /etc /etc/rc.d                  # Required for root.sh


2. kernel

echo > /etc/redhat-release << EOF
Red Hat Linux release 3.0 (drupal)
EOF

/etc/sysctl.conf

kernel.shmall = 2097152
kernel.shmmax = 1073741824
kernel.shmmni = 4096
kernel.sem = 250 32000 100 128
fs.file-max = 131072
net.ipv4.ip_local_port_range = 1024 65000
net.core.rmem_default = 1048576
net.core.rmem_max = 1048576
net.core.wmem_default = 262144
net.core.wmem_max = 262144

/sbin/sysctl -a | grep sem
/sbin/sysctl -a | grep shm
/sbin/sysctl -a | grep file-max
/sbin/sysctl -a | grep ip_local_port_range

/sbin/sysctl -a | grep rmem_default
/sbin/sysctl -a | grep rmem_max
/sbin/sysctl -a | grep wmem_default
/sbin/sysctl -a | grep wmem_max

3. limits
/etc/security/limits.conf
oracle              soft    nproc   2047
oracle              hard    nproc   16384
oracle              soft    nofile  1024
oracle              hard    nofile  65536
oracle              soft    stack   32768
oracle              hard    stack   32768

4. raw
export DBCA_RAW_CONFIG=$ORACLE_BASE/oradata/dbname/dbname_raw.conf
system=/dev/raw/db_system    # 256
users=/dev/raw/db_users      # 128
temp=/dev/raw/db_tmp         # 128
undotbs1=/dev/raw/db_undo_01 # 128
control1=/dev/raw/db_ctrl_1  # 128
control2=/dev/raw/db_ctrl_2  # 128
redo1_1=/dev/raw/db_redo1_1  # 128
redo1_2=/dev/raw/db_redo1_2  # 128
spfile=/dev/raw/db_spfile    # 4

lvcreate -L 256M -n db_system   vg00
lvcreate -L 128M -n db_users    vg00
lvcreate -L 128M -n db_tmp      vg00
lvcreate -L 128M -n db_undo_01  vg00
lvcreate -L 128M -n db_ctrl_1   vg00
lvcreate -L 128M -n db_ctrl_2   vg00
lvcreate -L 128M -n db_redo1_1  vg00
lvcreate -L 128M -n db_redo1_2  vg00
lvcreate -L 4M   -n db_spfile   vg00


mknod /dev/raw/db_system c 162 1
raw /dev/raw/db_system /dev/mapper/vg00-db_system

mknod /dev/raw/db_users c 162 2
raw /dev/raw/db_users /dev/mapper/vg00-db_users

mknod /dev/raw/db_tmp c 162 3
raw /dev/raw/db_tmp /dev/mapper/vg00-db_tmp

mknod /dev/raw/db_undo_01 c 162 4
raw /dev/raw/db_undo_01 /dev/mapper/vg00-db_undo_01

mknod /dev/raw/db_ctrl_1 c 162 5
raw /dev/raw/db_ctrl_1 /dev/mapper/vg00-db_ctrl_1

mknod /dev/raw/db_ctrl_2 c 162 6
raw /dev/raw/db_ctrl_2 /dev/mapper/vg00-db_ctrl_2

mknod /dev/raw/db_redo1_1 c 162 7
raw /dev/raw/db_redo1_1 /dev/mapper/vg00-db_redo1_1

mknod /dev/raw/db_redo1_2 c 162 8
raw /dev/raw/db_redo1_2 /dev/mapper/vg00-db_redo1_2

mknod /dev/raw/db_spfile c 162 9
raw /dev/raw/db_spfile /dev/mapper/vg00-db_spfile

