/*
 * Compile with
 *      gcc -shared -fpic -O2 -o libcwait.so libcwait.c
 * and use it by adding
 *      LD_PRELOAD=/path/to/libcwait.so
 # in the environment of the application with the bug.
 */

#include <errno.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
pid_t
__libc_wait (int *status)
{
  int res;
  asm volatile ("pushl %%ebx\n\t"
                "movl %2, %%ebx\n\t"
                "movl %1, %%eax\n\t"
                "int $0x80\n\t"
                "popl %%ebx"
                : "=a" (res)
                : "i" (__NR_wait4), "0" (WAIT_ANY), "c" (status), "d" (0), "S" (0));
  return res;
}
