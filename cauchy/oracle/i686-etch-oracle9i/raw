#! /bin/sh
#
# rawdevices       This shell script assignes rawdevices to block devices
#
# chkconfig: 345 56 44
# description: This scripts assignes raw devices to block devices.
# config: /etc/default/rawdevices

### BEGIN INIT INFO
# Provides:          raw
# Default-Start:     S 2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

[ -f /sbin/modprobe ] || exit 1
[ -f /bin/mknod ] || exit 1
[ -f /sbin/raw ] || exit 1
[ -f /etc/default/rawdevices ] || exit 1

if [ "$UID" -ne "0" ]; then
    echo "Must be root to run this script."
    exit 1
fi

/sbin/modprobe raw
[ -f /dev/raw ] && /bin/rm -f /dev/raw
[ -d /dev/raw ] || (/bin/rm -f /dev/raw && /bin/mkdir /dev/raw)
/bin/chmod 0755 /dev/raw

function bind_raw()
{
    first=1
    LC_ALL=C egrep -v '^ *#' /etc/default/rawdevices |
    while read RAW BLOCK MODE OWNER MINOR TAIL; do
        # echo [$RAW], [$BLOCK], [$MODE], [$OWNER], [$MINOR], [$TAIL]
        if [ -n "$RAW" -a -n "$BLOCK" ]; then
            if [ $first -eq 0 ]; then
                echo
            else
                first=0
            fi
            if [ ! -c $RAW ]; then
                /bin/rm -f $RAW
                /bin/mknod $RAW c 162 $MINOR
            fi
            rs=`/sbin/raw $RAW $BLOCK`
            echo "    $RAW  -->   $BLOCK";
            if [ -n "$MODE" ]; then
                echo "    $RAW  -->   $MODE";
                /bin/chmod $MODE $RAW
            fi
            if [ -n "$OWNER" ]; then
                echo "    $RAW  -->   $OWNER";
                /bin/chown $OWNER $RAW
            fi
        fi
    done
}

function unbind_raw()
{
    LC_ALL=C egrep -v '^ *#' /etc/default/rawdevices |
    while read RAW BLOCK MODE OWNER MINOR TAIL; do
        # echo [$RAW], [$BLOCK], [$MODE], [$OWNER], [$MINOR], [$TAIL]
        if [ -n "$RAW" -a -n "$BLOCK" -a -c  $RAW ]; then
            rs=`/sbin/raw $RAW 0 0`
            echo "    $RAW";
            /bin/rm -f $RAW
        fi
    done
}

# See how we were called.
case "$1" in
    start)
        # Assign devices
        echo $"Binding devices: "
        bind_raw
        echo $"done"
        ;;
    stop)
        # No action to be taken here
        echo $"Unbinding devices: "
        unbind_raw
        echo $"done"
        ;;
    status)
        ID=`id -u`
        if [ $ID -eq 0 ]; then
          /sbin/raw -qa
        else
          echo $"You need to be root to use this command ! "
        fi
        ;;
    restart|reload)
        $0 start
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart}"
        exit 1
esac

exit 0
