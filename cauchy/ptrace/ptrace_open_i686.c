#include <stdio.h>
#include <stdlib.h>
#include <sys/ptrace.h>
#include <sys/wait.h>

#define die(msg) { perror(msg); exit(EXIT_FAILURE); }

int
main(int argc, char *argv[])
{
    int pid, c, rc, status;
    struct timeval tv, tv2;

    if (argc <= 1) {
        printf("no enough args\n");
        exit(EXIT_FAILURE);
    }

    pid = fork();
    if (pid == -1) {
        die("fork");
    } else if (pid == 0) {
        if (ptrace(PTRACE_TRACEME, 0, (char *) 1, 0) == -1) die("trace me");
        if (kill(getpid(), SIGSTOP) == -1) die("stop");
        if(argc == 2) {
            execvp(argv[1], NULL);
        } else {
            execvp(argv[1], &argv[2]);
        }
        die("execvp");
    }

    c = 0;
    while(1) {
        c++;
        gettimeofday(&tv, NULL);
        rc = ptrace(PTRACE_SYSCALL, pid, (char *) 1, 0);
        if (rc == -1) die("trace syscall");

        /* 0x57F */
        rc = waitpid(pid, &status, 0);
        if (rc == -1) die("waitpid");

        gettimeofday(&tv2, NULL);

        if (WIFEXITED(status)) break;

        fprintf(stderr, "%03d: %07.5lf\n", c, (tv2.tv_sec - tv.tv_sec) + (tv2.tv_usec - tv.tv_usec) / 1E6);
    }

    return EXIT_SUCCESS;
}
