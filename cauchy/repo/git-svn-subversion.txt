#
# git-svn init --stdlayout --prefix=svn/ \
#   --rewrite-root=https://svn.collab.net/repos/svn \
#   file:///home/cauchy/wc/repos/subversion
#
# vi .git/config
#   fetch = developer-resources:refs/remotes/svn/developer-resources
#   fetch = svn-logos:refs/remotes/svn/svn-logos
#

[core]
        bare = false
        compression = 9
        filemode = true
        logallrefupdates = true
        repositoryformatversion = 0
[color]
        branch = true
        diff = auto
        status = auto
[diff]
        color = auto
[pack]
        compression = 9
        window = 128
        depth = 64
[svn]
        repack = 1000
        repackflags = -a -d
[svn-remote "svn"]
        rewriteRoot = https://svn.collab.net/repos/svn
        url = file:///home/cauchy/wc/repos/subversion
        fetch = trunk:refs/remotes/svn/trunk
        fetch = developer-resources:refs/remotes/svn/developer-resources
        fetch = svn-logos:refs/remotes/svn/svn-logos
        branches = branches/*:refs/remotes/svn/*
        tags = tags/*:refs/remotes/svn/tags/*
