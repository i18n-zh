#
# git-svn init --stdlayout --prefix=tsvn/ \
#   http://tortoisesvn.tigris.org/svn/tortoisesvn
#

[core]
        bare = false
        compression = 9
        filemode = true
        logallrefupdates = true
        repositoryformatversion = 0
[color]
        branch = true
        diff = auto
        status = auto
[diff]
        color = auto
[pack]
        compression = 9
        window = 128
        depth = 64
[svn]
        repack = 1000
        repackflags = -a -d
[svn-remote "svn"]
        url = http://tortoisesvn.tigris.org/svn/tortoisesvn
        fetch = trunk:refs/remotes/tsvn/trunk
        branches = branches/*:refs/remotes/tsvn/*
        tags = tags/*:refs/remotes/tsvn/tags/*
