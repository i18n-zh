autobook:
cvs -d :pserver:anoncvs@sources.redhat.com:/cvs/autobook -d autobook autobook

binutils:
cvs -d :pserver:anoncvs@sourceware.org:/cvs/src co -d binutils src

grub2:
cvs -d :pserver:anonymous@cvs.savannah.gnu.org:/sources/grub -d grub2 grub2

lame:
cvs -d :pserver:anonymous@lame.cvs.sourceforge.net:/cvsroot/lame -d lame lame

libupnp:
cvs -d :pserver:anonymous@upnp.cvs.sourceforge.net:/cvsroot/upnp -d libupnp libupnp

linux-igd:
cvs -d :pserver:anonymous@linux-igd.cvs.sourceforge.net:/cvsroot/linux-igd -d linux-igd linux-igd

unh-iscsi:
cvs -d :pserver:anonymous@unh-iscsi.cvs.sourceforge.net:/cvsroot/unh-iscsi -d unh-iscsi unh-iscsi
