debian-installer:
svn://svn.debian.org/svn/d-i/trunk

ejabberd:
http://svn.process-one.net/ejabberd/trunk

fop
http://svn.apache.org/repos/asf/xmlgraphics/fop/trunk

gcc 4.x:
svn://gcc.gnu.org/svn/gcc/branches/gcc-4_1-branch
svn://gcc.gnu.org/svn/gcc/branches/gcc-4_2-branch

Hibernate:
http://anonhibernate.labs.jboss.com/core/trunk/core
http://anonhibernate.labs.jboss.com/core/branches/Branch_3_2

iscsitarget/IET:
 svn://svn.berlios.de/iscsitarget/trunk
http://svn.berlios.de/svnroot/repos/iscsitarget/trunk
http://svn.berlios.de/viewcvs/iscsitarget

mailfs:
https://mailfs.svn.sourceforge.net/svnroot/mailfs/trunk

merge:
http://www.foresee.com.cn:8888/svn/merge/trunk

mpfr:
svn://scm.gforge.inria.fr/svn/mpfr/trunk
svn://scm.gforge.inria.fr/svn/mpfr/branches/2.3

neon:
http://svn.webdav.org/repos/projects/neon/trunk
http://svn.webdav.org/repos/projects/neon/branches/0.27.x

subversion:
https://svn.collab.net/repos/svn/trunk
https://svn.collab.net/repos/svn/branches/1.4.x

tortoisesvn:
http://tortoisesvn.tigris.org/svn/tortoisesvn/trunk
http://tortoisesvn.tigris.org/svn/tortoisesvn/branches/1.4.x
