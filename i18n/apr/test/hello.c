/*
 * lsbcc3 -march=i686 -O2 hello.c
 *
 * $ ldd a.out
 *     linux-gate.so.1 =>  (0xffffe000)
 *     libpthread.so.0 => /lib/tls/i686/cmov/libpthread.so.0 (0xb7f36000)
 *     libm.so.6 => /lib/tls/i686/cmov/libm.so.6 (0xb7f11000)
 *     libc.so.6 => /lib/tls/i686/cmov/libc.so.6 (0xb7ddf000)
 *     /lib/ld-lsb.so.3 (0xb7f56000)
 *
 */
#include <stdio.h>

int
main(int argc, char *argv[])
{
    fprintf(stdout, "Hello, World !\n");

    return 0;
}
