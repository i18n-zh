
################################ Update ################################
http://cvs.gnupg.org/cgi-bin/viewcvs.cgi/

svn cat svn://cvs.gnupg.org/gnupg/trunk/po/zh_CN.po > zh_CN.po
svn cat svn://cvs.gnupg.org/gnupg/branches/STABLE-BRANCH-1-4/po/zh_CN.po > zh_CN-1.4.x.po

make gnupg2.pot-update
make gnupg.pot-update


################################ Check ################################
    msgfmt -c zh_CN.po
    msgfmt -c zh_CN-1.4.x.po

    
################################ Format ################################
zh_CN.po:
    msgmerge --no-wrap -o zh_CN-new.po zh_CN.po zh_CN.pot
    diff zh_CN.po zh_CN-new.po 
    mv -f zh_CN-new.po zh_CN.po
    
zh_CN-1.4.x.po:
    msgmerge --no-wrap -o zh_CN-1.4.x-new.po zh_CN-1.4.x.po zh_CN-1.4.x.pot
    diff zh_CN-1.4.x.po zh_CN-1.4.x-new.po 
    mv -f zh_CN-1.4.x-new.po zh_CN-1.4.x.po


################################ Merge ################################
    msgmerge --no-wrap -o zh_CN-1.4.x-new.po zh_CN.po zh_CN-1.4.x.po
    mv -f zh_CN-1.4.x-new.po zh_CN-1.4.x.po
