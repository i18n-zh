# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: neon@webdav.org\n"
"POT-Creation-Date: 2007-07-14 21:26+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/ne_207.c:198
#, c-format
msgid ""
"Invalid HTTP status line in status element at line %d of response:\n"
"Status line was: %s"
msgstr ""

#: src/ne_auth.c:462
#, c-format
msgid "GSSAPI authentication error (%s)"
msgstr ""

#: src/ne_auth.c:523
#, c-format
msgid "Negotiate response verification failed: invalid response header token"
msgstr ""

#: src/ne_auth.c:596
#, c-format
msgid "Unknown algorithm in Digest authentication challenge"
msgstr ""

#: src/ne_auth.c:601
#, c-format
msgid "Incompatible algorithm in Digest authentication challenge"
msgstr ""

#: src/ne_auth.c:607
#, c-format
msgid "Missing nonce or realm in Digest authentication challenge"
msgstr ""

#: src/ne_auth.c:887
#, c-format
msgid "Digest mutual authentication failure: missing parameters"
msgstr ""

#: src/ne_auth.c:892
#, c-format
msgid "Digest mutual authentication failure: client nonce mismatch"
msgstr ""

#: src/ne_auth.c:897
#, c-format
msgid "Digest mutual authentication failure: nonce count mismatch (%u not %u)"
msgstr ""

#: src/ne_auth.c:937
#, c-format
msgid "Digest mutual authentication failure: request-digest mismatch"
msgstr ""

#: src/ne_basic.c:94
#, c-format
msgid "Could not determine file size: %s"
msgstr ""

#: src/ne_basic.c:149
#, c-format
msgid "Response did not include requested range"
msgstr ""

#: src/ne_basic.c:183
#, c-format
msgid "Range is not satisfiable"
msgstr ""

#: src/ne_basic.c:188
#, c-format
msgid "Resource does not support ranged GETs."
msgstr ""

#: src/ne_compress.c:173
#, c-format
msgid "%s: %s"
msgstr ""

#: src/ne_compress.c:184
#, c-format
msgid "%s: %s (code %d)"
msgstr ""

#: src/ne_compress.c:232
msgid "Could not inflate data"
msgstr ""

#: src/ne_compress.c:293
msgid "Could not initialize zlib"
msgstr ""

#: src/ne_gnutls.c:521 src/ne_openssl.c:386
#, c-format
msgid "Server certificate was missing commonName attribute in subject name"
msgstr ""

#: src/ne_gnutls.c:557 src/ne_openssl.c:599
#, c-format
msgid "SSL negotiation failed: %s"
msgstr ""

#: src/ne_gnutls.c:566
#, c-format
msgid "Server did not send certificate chain"
msgstr ""

#: src/ne_locks.c:584
msgid "LOCK response missing Lock-Token header"
msgstr ""

#: src/ne_locks.c:759
#, c-format
msgid "Response missing activelock for %s"
msgstr ""

#: src/ne_locks.c:801
#, c-format
msgid "No activelock for <%s> returned in LOCK refresh response"
msgstr ""

#: src/ne_openssl.c:418
#, c-format
msgid "Certificate verification error: %s"
msgstr ""

#: src/ne_openssl.c:618
#, c-format
msgid "SSL server did not present certificate"
msgstr ""

#: src/ne_openssl.c:627
#, c-format
msgid "Server certificate changed: connection intercepted?"
msgstr ""

#: src/ne_props.c:371 src/ne_props.c:411
msgid "Response exceeds maximum property count"
msgstr ""

#: src/ne_redirect.c:92
#, c-format
msgid "Could not parse redirect location."
msgstr ""

#: src/ne_request.c:237
#, c-format
msgid "%s: connection was closed by proxy server."
msgstr ""

#: src/ne_request.c:240
#, c-format
msgid "%s: connection was closed by server."
msgstr ""

#: src/ne_request.c:245
#, c-format
msgid "%s: connection timed out."
msgstr ""

#: src/ne_request.c:351
msgid "offset invalid"
msgstr ""

#: src/ne_request.c:356
#, c-format
msgid "Could not seek to offset %s of request body file: %s"
msgstr ""

#: src/ne_request.c:401
msgid "Could not send request body"
msgstr ""

#: src/ne_request.c:759
msgid "Could not read chunk size"
msgstr ""

#: src/ne_request.c:766
msgid "Could not parse chunk size"
msgstr ""

#: src/ne_request.c:803
msgid "Could not read response body"
msgstr ""

#: src/ne_request.c:819
msgid "Could not read chunk delimiter"
msgstr ""

#: src/ne_request.c:822
msgid "Chunk delimiter was invalid"
msgstr ""

#: src/ne_request.c:928
msgid "Could not read status line"
msgstr ""

#: src/ne_request.c:950
msgid "Could not parse response status line."
msgstr ""

#: src/ne_request.c:962
msgid "Could not read interim response headers"
msgstr ""

#: src/ne_request.c:996
msgid "Could not send request"
msgstr ""

#: src/ne_request.c:1044 src/ne_request.c:1062 src/ne_request.c:1072
msgid "Error reading response headers"
msgstr ""

#: src/ne_request.c:1090
#, c-format
msgid "Response header too long"
msgstr ""

#: src/ne_request.c:1172
msgid "Response exceeded maximum number of header fields."
msgstr ""

#: src/ne_request.c:1189
#, c-format
msgid "Could not resolve hostname `%s': %s"
msgstr ""

#: src/ne_request.c:1309
msgid "Invalid Content-Length in response"
msgstr ""

#: src/ne_request.c:1371
#, c-format
msgid "Could not write to file: %s"
msgstr ""

#: src/ne_request.c:1444
#, c-format
msgid "Could not create SSL connection through proxy server: %s"
msgstr ""

#: src/ne_request.c:1491
#, c-format
msgid "Could not create socket"
msgstr ""

#: src/ne_request.c:1535
msgid "Could not connect to server"
msgstr ""

#: src/ne_request.c:1538
msgid "Could not connect to proxy server"
msgstr ""

#: src/ne_session.c:305 src/ne_session.c:316
msgid "[invalid date]"
msgstr ""

#: src/ne_session.c:329
msgid "certificate is not yet valid"
msgstr ""

#: src/ne_session.c:330
msgid "certificate has expired"
msgstr ""

#: src/ne_session.c:331
msgid "certificate issued for a different hostname"
msgstr ""

#: src/ne_session.c:332
msgid "issuer is not trusted"
msgstr ""

#: src/ne_session.c:337
msgid "Server certificate verification failed: "
msgstr ""

#: src/ne_socket.c:475 src/ne_socket.c:529 src/ne_socket.c:632
msgid "Connection closed"
msgstr ""

#: src/ne_socket.c:539 src/ne_socket.c:644
msgid "Secure connection truncated"
msgstr ""

#: src/ne_socket.c:551 src/ne_socket.c:656
#, c-format
msgid "SSL error: %s"
msgstr ""

#: src/ne_socket.c:554
#, c-format
msgid "SSL error code %d/%d/%lu"
msgstr ""

#: src/ne_socket.c:637
#, c-format
msgid "SSL alert received: %s"
msgstr ""

#: src/ne_socket.c:652
msgid "SSL socket read failed"
msgstr ""

#: src/ne_socket.c:751
msgid "Line too long"
msgstr ""

#: src/ne_socket.c:891 src/ne_socket.c:897
msgid "Host not found"
msgstr ""

#: src/ne_socket.c:1007
msgid "Socket descriptor number exceeds FD_SETSIZE"
msgstr ""

#: src/ne_socket.c:1219
msgid "Client certificate verification failed"
msgstr ""

#: src/ne_socket.c:1235
msgid "SSL disabled due to lack of entropy"
msgstr ""

#: src/ne_socket.c:1242
msgid "SSL disabled due to library version mismatch"
msgstr ""

#: src/ne_socket.c:1248
msgid "Could not create SSL structure"
msgstr ""

#: src/ne_xml.c:280
#, c-format
msgid "XML parse error at line %d: invalid element name"
msgstr ""

#: src/ne_xml.c:436
msgid "Unknown error"
msgstr ""

#: src/ne_xml.c:521
msgid "Invalid Byte Order Mark"
msgstr ""

#: src/ne_xml.c:609
#, c-format
msgid "XML parse error at line %d: %s."
msgstr ""

#: src/ne_xmlreq.c:31
#, c-format
msgid "Could not parse response: %s"
msgstr ""
