neon trunk:
    95 translated messages.

    wget -O neon.pot http://svn.webdav.org/repos/projects/neon/trunk/po/neon.pot
    msgmerge --no-wrap -o zh_CN_new.po zh_CN.po neon.pot
    /bin/mv -f zh_CN_new.po zh_CN.po
    msgfmt --statistics -c -o zh_CN.mo zh_CN.po

neon 0.28.x:
    95 translated messages.

    wget -O neon-0.28.x.pot http://svn.webdav.org/repos/projects/neon/branches/0.28.x/po/neon.pot
    msgmerge --no-wrap -o zh_CN-0.28.x_new.po zh_CN-0.28.x.po neon-0.28.x.pot
    /bin/mv -f zh_CN-0.28.x_new.po zh_CN-0.28.x.po
    msgfmt --statistics -c -o zh_CN-0.28.x.mo zh_CN-0.28.x.po

neon 0.27.x:
    96 translated messages.

    wget -O neon-0.27.x.pot http://svn.webdav.org/repos/projects/neon/branches/0.27.x/po/neon.pot
    msgmerge --no-wrap -o zh_CN-0.27.x_new.po zh_CN-0.27.x.po neon-0.27.x.pot
    /bin/mv -f zh_CN-0.27.x_new.po zh_CN-0.27.x.po
    msgfmt --statistics -c -o zh_CN-0.27.x.mo zh_CN-0.27.x.po

neon 0.26.x:
    78 translated messages.

    wget -O neon-0.26.x.pot http://svn.webdav.org/repos/projects/neon/branches/0.26.x/po/neon.pot
    msgmerge --no-wrap -o zh_CN-0.26.x_new.po zh_CN-0.26.x.po neon-0.26.x.pot
    /bin/mv -f zh_CN-0.26.x_new.po zh_CN-0.26.x.po
    msgfmt --statistics -c -o zh_CN-0.26.x.mo zh_CN-0.26.x.po
