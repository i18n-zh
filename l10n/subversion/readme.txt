subversion trunk:
    1798 translated messages.

    msgmerge --no-wrap -o zh_CN_new.po zh_CN.po subversion.pot
    /bin/mv -f zh_CN_new.po zh_CN.po
    msgfmt --statistics -c -o zh_CN.mo zh_CN.po

subversion 1.5.x:
    1793 translated messages.

    msgmerge --no-wrap -o zh_CN-1.5.x_new.po zh_CN-1.5.x.po subversion-1.5.x.pot
    /bin/mv -f zh_CN-1.5.x_new.po zh_CN-1.5.x.po
    msgfmt --statistics -c -o zh_CN-1.5.x.mo zh_CN-1.5.x.po

subversion 1.4.x:
    1492 translated messages.

    msgmerge --no-wrap -o zh_CN-1.4.x_new.po zh_CN-1.4.x.po subversion-1.4.x.pot
    /bin/mv -f zh_CN-1.4.x_new.po zh_CN-1.4.x.po
    msgfmt --statistics -c -o zh_CN-1.4.x.mo zh_CN-1.4.x.po
