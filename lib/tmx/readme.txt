*) Normalization PO Files
    msgmerge --no-wrap -o tmp.po zh_CN.po project.pot
    mv -f tmp.po zh_CN.po
    msgfmt --statistics -c zh_CN.po

*) Concatenate PO Files
    msgcat --use-first -o superset_zh_CN.po file1.po file2.po

*) Initialize a New Translation File
    msgmerge --compendium superset_zh_CN.po -o file.po /dev/null file.pot

*) Update an Existing Translation File
    msgcat --use-first -o tmp.po superset_zh_CN.po file.po
    msgattrib --no-fuzzy --no-obsolete  --translated  --no-location \
        --sort-output --no-wrap tmp.po > superset_zh_CN.po
    /bin/rm tmp.po

    msgmerge superset_zh_CN.po file.pot | msgattrib --no-obsolete > file.po
